# gitlabrb_sanitizer

Sanitizes the `gitlab.rb` file by redacting and replacing sensitive config variables.

### Usage

```shell
cd /tmp
/opt/gitlab/embedded/bin/git clone --depth=1 https://gitlab.com/gitlab-com/support/toolbox/gitlabrb_sanitizer.git
cd gitlabrb_sanitizer
less sanitizer # Please review before executing
/opt/gitlab/embedded/bin/ruby sanitizer # --file /etc/gitlab/gitlab.rb is the default
```

Please find more options via `/opt/gitlab/embedded/bin/ruby sanitizer --help`
or [in the `class ConfigurationParser` code](sanitizer#L50).


#### Execute Directly

**Please review script before executing anything directly into sudo!**

```sh
curl -s https://gitlab.com/gitlab-com/support/toolbox/gitlabrb_sanitizer/-/raw/master/sanitizer | sudo /opt/gitlab/embedded/bin/ruby
```

### Alternatives

See [our "How do I scrub sensitive information" documentation](https://about.gitlab.com/support/sensitive-information.html#how-do-i-scrub-sensitive-information).
